import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/constants.dart';
import 'package:flutter_ui_exercises/views/pages/home_page.dart';
import 'package:flutter_ui_exercises/views/pages/onboardingPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI Exercises @Maftah',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: HomePage(),
      routes: {homePage: ((_) => HomePage())},
    );
  }
}
