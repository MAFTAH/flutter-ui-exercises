// import 'dart:ui';


import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class OnboardingModel {
  final String image;
  final String title;
  final String subtitle;
  final Color pageColor;
  final Color titleColor;

  OnboardingModel(
      {@required this.image,
      @required this.title,
      @required this.subtitle,
      @required this.pageColor,
      @required this.titleColor});
}

List<OnboardingModel> onboardingData = <OnboardingModel>[
  OnboardingModel(
      image: 'assets/me-coding.PNG',
      pageColor: Colors.orange[100],
      subtitle: "Simply Innovative.",
      title: "Welcome to Qlicue Digital Agency",
      titleColor: Colors.lightBlue),
  OnboardingModel(
      image: 'assets/avocado.png',
      pageColor: Colors.orange[200],
      subtitle: "Practical Training One was so amaizing.",
      title: "One Avocado unakula wewe na Robbie",
      titleColor: Colors.green[900]),
  OnboardingModel(
      image: 'assets/rose-1706449_1280.jpg',
      pageColor: Colors.orange[300],
      subtitle: "Organize your materials, Waite wana, Disc lianze.",
      title: "Let's focus on EV this week, Code zipo tu",
      titleColor: Colors.red),
];
