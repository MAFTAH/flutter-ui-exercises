import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Vidumu {
  final Color color;
  final String picture;
  final String menuType;
  final String firstText;
  final String secondText;
  final String thirdText;
  final String price;
  final String currency;

  Vidumu(
      {@required this.picture,
      @required this.menuType,
      @required this.firstText,
      @required this.secondText,
      @required this.thirdText,
      @required this.price,
      @required this.currency,
      @required this.color});
}

List<Vidumu> vidumu = <Vidumu>[
  Vidumu(
      color: Colors.orange[400],
      firstText: "Bread",
      menuType: "Breakfast",
      price: "525",
      secondText: "Peanut butter,",
      thirdText: "Apple",
      currency: "Tsh",
      picture: "assets/avocado.png"),
  Vidumu(
      color: Colors.deepPurple,
      firstText: "Salmon",
      menuType: "Lunch",
      price: "602",
      secondText: "Mixed Vaggles",
      thirdText: "Avocado",
      currency: "Tsh",
      picture: "assets/kidumu1.png"),
  Vidumu(
      color: Colors.pink,
      firstText: "Recommended",
      menuType: "Dinner",
      price: "800",
      secondText: "Mixed Vaggles",
      thirdText: "Mango",
      currency: "Tsh",
      picture: "assets/tikiti.png")
];
