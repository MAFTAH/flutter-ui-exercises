import 'package:flutter/foundation.dart';

class AnimatedPicture {
  final String cover;
  final String title;
  final String price;

  AnimatedPicture({@required this.cover, @required this.title, @required this.price});
}

List<AnimatedPicture> animatedPictures = <AnimatedPicture>[
  AnimatedPicture(
      cover: "assets/girl-1848454_1280.jpg",
      title: "Beautiful Cardigan",
      price: "\$600"),
  AnimatedPicture(
      cover: "assets/kidumu2.jpg",
      title: "Quality Watches",
      price: "\$400"),
  AnimatedPicture(
      cover: "assets/rose-1706449_1280.jpg",
      title: "White Beautiful Bag",
      price: "\$350")
];
