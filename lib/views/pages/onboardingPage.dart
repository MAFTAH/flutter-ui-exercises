import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/models/onboarding_model.dart';
import 'package:flutter_ui_exercises/views/pages/login_page.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class OnboardingPage extends StatelessWidget {
  final pages = [
    PageViewModel(
      pageColor: onboardingData[0].pageColor,
      bubbleBackgroundColor: Colors.deepPurple,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(
            onboardingData[0].title,
            style: TextStyle(color: onboardingData[0].titleColor),
          ),
          Text(
            onboardingData[0].subtitle,
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
        ],
      ),
      mainImage: Image.asset(
        onboardingData[0].image,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: onboardingData[1].pageColor,
      bubbleBackgroundColor: Colors.deepPurple,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(
            onboardingData[1].title,
            style: TextStyle(color: onboardingData[1].titleColor),
          ),
          Text(
            onboardingData[1].subtitle,
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
        ],
      ),
      mainImage: Image.asset(
        onboardingData[1].image,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: onboardingData[2].pageColor,
      bubbleBackgroundColor: Colors.deepPurple,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(
            onboardingData[2].title,
            style: TextStyle(color: onboardingData[2].titleColor, fontSize: 21),
          ),
          Text(
            onboardingData[2].subtitle,
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
        ],
      ),
      mainImage: Image.asset(
        onboardingData[2].image,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
  ];

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          backgroundColor: Colors.orange,
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                IntroViewsFlutter(
                  pages,
                  onTapDoneButton: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                      return LoginPage();
                    }));
                  },
                  showSkipButton: false,
                  doneText: Text(
                    "Start Challenges",
                    style: TextStyle(color: Colors.deepPurple[800]),
                  ),
                  pageButtonsColor: Colors.deepPurple,
                  pageButtonTextStyles: new TextStyle(
                    color: Colors.deepPurple,
                    fontSize: 16.0,
                  ),
                ),
                Positioned(
                  top: 20.0,
                  left: MediaQuery.of(context).size.width / 2 - 50,
                  child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          image: DecorationImage(
                              image: AssetImage('assets/Qlicue.jpeg'),
                              fit: BoxFit.cover))),
                )
              ],
            ),
          ),
        );
      
  }
}
