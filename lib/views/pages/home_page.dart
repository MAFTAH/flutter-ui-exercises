import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/views/screens/drawer_page.dart';
import 'package:flutter_ui_exercises/views/screens/first_screen.dart';
import 'package:flutter_ui_exercises/views/screens/fourth_screen.dart';
import 'package:flutter_ui_exercises/views/screens/second_screen.dart';
import 'package:flutter_ui_exercises/views/screens/third_screen.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> _screens = [
      FirstScreen(),
      SecondScreen(),
      ThirdScreen(),
      FourthScreen()
    ];
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'UI EXERCISES',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              'اللهم ربنا ارزقني رزقا حلالا طيبا',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        centerTitle: true,
      ),
      drawer: DrawerPage(),
      
      body: _screens[_selectedIndex],
      bottomNavigationBar: BottomNavyBar(
        iconSize: 30,
        currentIndex: _selectedIndex,
        onItemSelected: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
            icon: Icon(Icons.home),
            title: Text('Nawal'),
            activeColor: Colors.deepPurple,
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.book),
              title: Text('Eliza'),
              activeColor: Colors.deepOrange),
          BottomNavyBarItem(
              icon: Icon(Icons.chat),
              title: Text('Vicky'),
              activeColor: Colors.green),
          BottomNavyBarItem(
              icon: Icon(Icons.info),
              title: Text('Chichi'),
              activeColor: Colors.black)
        ],
      ),
    );
  }
}
