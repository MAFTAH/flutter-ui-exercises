import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/models/vidumu.dart';

class VidumuCard extends StatelessWidget {
  final Vidumu kidumu;

  const VidumuCard({Key key, @required this.kidumu}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 15, top: 28),
            child: Container(
              height: 190,
              width: 120,
              decoration: BoxDecoration(
                  color: kidumu.color,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(70),
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.only(top: 65, left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      kidumu.menuType,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Text(
                      kidumu.firstText,
                      style: TextStyle(color: Colors.white, fontSize: 8),
                    ),
                    Text(
                      kidumu.secondText,
                      style: TextStyle(color: Colors.white, fontSize: 8),
                    ),
                    Text(
                      kidumu.thirdText,
                      style: TextStyle(color: Colors.white, fontSize: 8),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          kidumu.price,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          kidumu.currency,
                          style: TextStyle(color: Colors.white, fontSize: 8),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5,),
            child: CircleAvatar(
              radius: 42,
              backgroundColor: Colors.blue[50].withOpacity(0.4),
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    image: DecorationImage(
                        image: AssetImage(kidumu.picture),
                        fit: BoxFit.cover)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
