import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class BatteryCapacityCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(50),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15))),
      child: Container(
        padding: EdgeInsets.only(left: 15, top: 25, bottom: 25, right: 15),
        height: 180,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(55),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 200,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text('2100 ',
                          style: TextStyle(
                            color: Colors.blue[900],
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          )),
                      Text('ml',
                          style: TextStyle(
                            color: Colors.blue[900],
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ))
                    ],
                  ),
                  Text('of daily goal 3.5L',
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Divider(
                    thickness: 1.5,
                    color: Colors.grey[350],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.access_time,
                        size: 15,
                        color: Colors.grey[600],
                      ),
                      Text(' Last drink 8:26 AM',
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.notifications,
                        size: 15,
                        color: Colors.pinkAccent[200],
                      ),
                      Text(' Your bottle is empty, refill it !',
                          style: TextStyle(
                            color: Colors.pinkAccent[200],
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ))
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 25, bottom: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CircleAvatar(
                    radius: 15,
                    child: FloatingActionButton(
                      backgroundColor: Colors.white,
                      elevation: 15,
                      child: Icon(
                        Icons.add,
                        color: Colors.blue[900],
                      ),
                      onPressed: () {},
                    ),
                  ),
                  CircleAvatar(
                    radius: 15,
                    child: FloatingActionButton(
                      
                      backgroundColor: Colors.white,
                      elevation: 15,
                      child: Icon(
                        Icons.remove,
                        color: Colors.blue[900],
                      ),
                      onPressed: () {},
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 124,
              width: 50,
              decoration: BoxDecoration(
                  // color: Colors.blue[900],
                  borderRadius: BorderRadius.circular(35)),
              // child: Column(
              //   children: <Widget>[
              //     Container(
              //       height: 50,
              //       decoration: BoxDecoration(
              //           borderRadius: BorderRadius.only(
              //               topLeft: Radius.circular(35),
              //               topRight: Radius.circular(35)),
              //           color: Colors.blue[100]),
              //     ),
              //     SizedBox(
              //       height: 20,
              //     ),
              //     Row(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       crossAxisAlignment: CrossAxisAlignment.start,
              //       children: <Widget>[
              //         Text(
              //           '60',
              //           style: TextStyle(color: Colors.white),
              //         ),
              //         Text('%',
              //             style: TextStyle(color: Colors.white, fontSize: 8))
              //       ],
              //     ),
              //   ],
              // ),

               child: RotatedBox(
              quarterTurns: 4,
              child: WaveWidget(
                config: CustomConfig(
                  gradients: [
                    [Colors.blue[900], Colors.blue[900]],
                    [Colors.blue[900], Colors.blue[900]],
                  ],
                  durations: [19500, 10800],
                  heightPercentages: [0.40, 0.41],
                  gradientBegin: Alignment.bottomLeft,
                  gradientEnd: Alignment.bottomRight,
                ),
                waveAmplitude: 2,
                size: Size(
                  50,
                  124,
                ),
              ),
            ),


            )
          ],
        ),
      ),
    );
  }
}


/**
 * RotatedBox(
              quarterTurns: 2,
              child: WaveWidget(
                config: CustomConfig(
                  gradients: [
                    [Colors.deepPurple, Colors.deepPurple.shade200],
                    [Colors.indigo.shade200, Colors.purple.shade200],
                  ],
                  durations: [19440, 10800],
                  heightPercentages: [0.20, 0.25],
                  blur: MaskFilter.blur(BlurStyle.solid, 10),
                  gradientBegin: Alignment.bottomLeft,
                  gradientEnd: Alignment.topRight,
                ),
                waveAmplitude: 0,
                size: Size(
                  double.infinity,
                  double.infinity,
                ),
              ),
            ),
 */