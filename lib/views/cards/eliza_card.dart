import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/models/animated_picture_model.dart';

class ElizaCard extends StatelessWidget {
  final AnimatedPicture eliza;

  const ElizaCard({Key key, @required this.eliza}) : super(key: key);
  @override
  Widget build(BuildContext context) {
      return Column(
      children: <Widget>[
        Container(
          height: 350,
          width: 250,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              image: DecorationImage(
                  image: AssetImage(eliza.cover), fit: BoxFit.cover)),
        ),
        
      ],
    );
  }
}
