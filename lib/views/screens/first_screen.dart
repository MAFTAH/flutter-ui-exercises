import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/models/vidumu.dart';
import 'package:flutter_ui_exercises/views/cards/battery_capacity_card.dart';
import 'package:flutter_ui_exercises/views/cards/vidumu_card.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue[50],
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: BatteryCapacityCard(),
                )
              ]),
            ),
            SliverToBoxAdapter(
              child: Container(
                height: 400,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return VidumuCard(
                      kidumu: vidumu[index],
                    );
                  },
                  itemCount: vidumu.length,
                ),
              ),
            )
          ],
        ));
  }
}
