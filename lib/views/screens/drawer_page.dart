import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/views/cards/oval_right_border_cliper.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: OvalRightBorderClipper(),
      child: Padding(
        padding: const EdgeInsets.only(top: 80, bottom: 56),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.orange,
            boxShadow: [
              BoxShadow(color: Colors.black45)
            ]
          ),width: 300,
          child: Column(
            children: <Widget>[
              UserAccountsDrawerHeader(
                currentAccountPicture: CircleAvatar(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(60),
                        image: DecorationImage(
                            image: AssetImage('assets/me.jpg'),
                            fit: BoxFit.cover)),
                  ),
                ),
                accountName: Text('Ally Maftah'),
                accountEmail: Text('maflampizzle69@gmail.com'),
              )
            ],
          ),
        ),
      ),
    );
  }
}









