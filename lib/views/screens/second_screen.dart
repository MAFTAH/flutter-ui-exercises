import 'package:flutter/material.dart';
import 'package:flutter_ui_exercises/models/animated_picture_model.dart';
import 'package:flutter_ui_exercises/views/cards/eliza_card.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            SizedBox(
              height: 20,
            ),
            Container(
              child: Text('Best Items Over Around'),
              alignment: Alignment.center,
            )
          ]),
        ),
        SliverToBoxAdapter(
            child: Padding(
          padding:  EdgeInsets.only(top: 15),
          child: Container(
            height: 380,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(left: 55),
                  child: ElizaCard(
                    eliza: animatedPictures[index],
                  ),
                );
              },
              itemCount: 3,
            ),
          ),
        )),
        SliverList(
          delegate: SliverChildListDelegate([
            // SizedBox(height: 10,),
            Text(
              animatedPictures[0].title,
              style: TextStyle(
                  color: Colors.deepOrange,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Text(animatedPictures[0].price,
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center)
          ]),
        )
      ],
    );
  }
}
